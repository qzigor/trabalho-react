import { ButtonCreateAccount, ButtonReturn, ContainerDaView, ContainerHeader, ContainerImage, GreenBarr, SectionInput, SectionInputPassword, TextDaViewConfirmarSenha, TextDaViewEmail, TextDaViewNome, TextDaViewSenha, TextInput, TextTitle, FirstTermOfUser, SecondTermOfUser, TextInputPassword } from "./style";
import {AiFillEyeInvisible} from 'react-icons/ai';
import {BiCheckbox} from 'react-icons/bi';
import React from "react";
import { useNavigation } from "@react-navigation/native";
import { Button } from "../components/Button/Button";
import { InputText } from "../components/InputText/InputText";

export function CreateAccount(){
    const navigation = useNavigation();
    return (
        <ContainerDaView>
            <GreenBarr></GreenBarr>

            <ContainerHeader>
                 <ContainerImage source = {require('../image/Logo 2.jpg')}/>
                <TextTitle>
                    Criar Conta
                </TextTitle>
            </ContainerHeader>

            <InputText
                text="Nome"
                isPassword = {true}
            />

            <InputText
                text="E-mail"
                isPassword = {true}
            />

            <InputText
                text="Senha"
                isPassword = {true}
            />

            <InputText
                text="Confirme sua Senha"
                isPassword = {true}
            />

            <FirstTermOfUser>
                <BiCheckbox size={30}/>
                Aceito receber novidades do Reus-e.
            </FirstTermOfUser>

            <SecondTermOfUser>
                <BiCheckbox size={50}/>
                Estou de acordo com os termos de uso e políticas de privacidade do Reus-e.
            </SecondTermOfUser>

            <Button
                text="Criar Conta"
            />

            <ButtonReturn
                onPress = {() => navigation.navigate('Login')}>
                Voltar
            </ButtonReturn>

        </ContainerDaView>

    )
}
export default CreateAccount;