import { View } from "react-native";
import { Container, ContainerImagem, Logo, Reuse, TextInput, SectionInputEmail, RememberPassword, SectionInputPassoword, ButtonEnter, LoginGoogle, LoginFacebook, Registration, Question, CreatAccount, GreenBarr, ContainerTextEmail, ContainerTextPassword,} from "./style";
import {AiFillEyeInvisible} from 'react-icons/ai';
import {AiFillGoogleCircle} from 'react-icons/ai';
import {AiFillFacebook} from 'react-icons/ai';
import React from "react";
import { useNavigation } from "@react-navigation/native";
import { Button } from "../components/Button/Button";
import { InputText } from "../components/InputText/InputText";

export function Login(){
    const navigation = useNavigation();
    return(
        <Container>
            <GreenBarr></GreenBarr>

            <ContainerImagem>
                <Logo source = {require('../image/Logo 2.png')} />
                <Reuse source = {require('../image/Reus-e.png')}/>
            </ContainerImagem>

            <InputText
                text="E-mail"
            />

            <InputText
                text="Senha"
            />

            <RememberPassword>
                Esqueci minha senha
            </RememberPassword>

            <Button
                text="Entrar"
            />

            <LoginGoogle>
                Entrar com Google
                <AiFillGoogleCircle/>
            </LoginGoogle>

            <LoginFacebook>
                Entrar com Facebook
                <AiFillFacebook/>
            </LoginFacebook>

            <Registration>
                <Question>
                    Não é cadastrado?
                </Question>
                <CreatAccount 
                    onPress = {() => navigation.navigate('CreateAccount')}>
                    Criar conta
                </CreatAccount>
            </Registration>
            <GreenBarr></GreenBarr>
        </Container>
    );
}

export default Login;


