import styled from "styled-components/native";
import {style} from '../global/style';

export const Container = styled.View`
    background-color: white;
`;

export const ContainerImagem = styled.View`
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const Reuse = styled.Image`
    width: 180px;
    height: 45px;
    margin-top: 50px;
`;

export const Logo = styled.Image`
    width: 125px;
    height: 119px;
    margin-top: 66px;
`;

export const SectionInputEmail = styled.View`
    background-color: rgba(10, 10, 10, 0.123);
    margin-left: 13.8%;
    margin-right: 13.8%;
    border-radius: 8;
    margin-bottom: 25;
    padding: 0.2rem;
`;  

export const SectionInputPassoword = styled.View`
    background-color: rgba(10, 10, 10, 0.123);
    margin-left: 13.8%;
    margin-right: 13.8%;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-radius: 8;
    margin-bottom: 8;
    padding: 0.2rem;
`

export const TextInput = styled.TextInput`
    font-family: ${style.fonts.rb};
    font-size: 19;
    margin-bottom: 8;
    align-items: center;
    justify-content: center;
`;

export const ContainerTextEmail = styled.Text`
    font-family: ${style.fonts.rb};
    margin-left: 13.8%;
    font-size: 32;
    margin-top: 60px;
`
export const ContainerTextPassword = styled.Text`
    font-family: ${style.fonts.rb};
    margin-left: 13.8%;
    font-size: 32;
`


export const RememberPassword = styled.Text`
    font-family: ${style.fonts.rb};
    margin-left: 13.8%;
    color: ${style.colors.lightgreen};
    font-size: 15;
    margin-bottom: 20;
`

export const ButtonEnter = styled.View`
    background-color: ${style.colors.lightgreen};
    color: black;
    width: 30%;
    height: 6%;
    margin-left: 38.4%;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 40;
    margin-bottom: 20;
    border-radius: 10;
`

export const LoginGoogle = styled.View`
    flex-direction: row;
    justify-content: space-evenly;
    font-family: ${style.fonts.rb};
    font-size: 25;
    margin-bottom: 10;
`


export const LoginFacebook = styled.View`
    flex-direction: row;
    justify-content: space-evenly;
    font-family: ${style.fonts.rb};
    font-size: 25;
    margin-bottom: 10%;
`

export const Registration = styled.View`
    flex-direction: row;
    justify-content: space-evenly;
`

export const Question = styled.View`
    font-family: ${style.fonts.rb};
    font-size: 19;
`


export const CreatAccount = styled.Text`
    font-family: ${style.fonts.rb};
    font-size: 19;
    color: ${style.colors.lightgreen};
`


export const ContainerDaView = styled.View`
   width: 100%;
   height: 100%;
   background-color: white;
`

export const TextTitle = styled.View`
    margin-top: 6.7%;
    margin-left: 17%;
    font-size: 30;
`

export const TextInputPassword = styled.TextInput`
    font-family: ${style.fonts.rb};
    font-size: 19;
    margin-bottom: 8;
    align-items: center;
    justify-content: center;
    padding-left: 2.0rem;
`;

export const ContainerImage = styled.Image`
    width: 14.4%;
    height: 50;
    margin-left: 3.0%;
    margin-top: 6.52%;
`

export const ContainerHeader = styled.View`
   flex-direction: row;
   align-items: center;
    
`

export const SectionInput = styled.View`
    background-color: white;
    margin-left: 13.8%;
    margin-right: 13.8%;
    width: 72%;
    height: 4.3%;
    border-radius: 12;
    justify-content: center;
    padding-left: 1.5rem;
    border-width: 2px;
`

export const SectionInputPassword = styled.View`
    background-color: white;
    margin-left: 13.8%;
    margin-right: 13.8%;
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    padding-right: 1.5rem;
    border-radius: 12;
    width: 72%;
    height: 4.3%;
    border-width: 2px;
`


export const TextDaViewNome = styled.Text`
    font-family: 'Poppins';
    font-size: 16;
    margin-top: 15.3%;
    margin-left: 14.4%;
    font-weight: bold;
`

export const TextDaViewEmail = styled.Text`
    font-family: 'Poppins';
    font-size: 16;
    margin-left: 14.4%;
    margin-top: 6%;
    font-weight: bold;
`

export const TextDaViewSenha = styled.Text`
    font-family: 'Poppins';
    font-size: 16;
    margin-left: 14.4%;
    margin-top: 6%;
    font-weight: bold;
    
`

export const TextDaViewConfirmarSenha = styled.Text`
    font-family: 'Poppins';
    font-size: 16;
    margin-left: 14.4%;
    margin-top: 6%;
    font-weight: bold;
`

export const FirstTermOfUser = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 60%;
    height: 10%;
    margin-left: 14.4%;
    margin-top: 20px;
    font-family: 'Roboto';
    font-size: 17;
    justify-content: space-between;
`

export const SecondTermOfUser = styled.View`
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    width: 70%;
    height: 10%;
    margin-left: 14.4%;
    font-family: 'Roboto';
    font-size: 17;
`

export const ButtonCreateAccount = styled.View`
    font-family: 'Roboto';
    font-size: 30;
    background-color: #2ECB72;
    width: 59%;
    justify-content: center;
    align-items: center;
    margin-left: 21.4%;
    border-radius: 10;
    margin-top: 1.5rem;
    padding: 0.5rem;
`

export const ButtonReturn = styled.Text`
    font-family: 'Roboto';
    font-size: 15;
    margin-top: 1.5rem;
    background-color: #b5dac5;
    width: 19.2%;
    margin-left: 40%;
    padding: 0.5rem;
    border-radius: 7;
    display: flex;
    justify-content: center;
    align-items:center;
`

export const GreenBarr = styled.View`
    background-color: #2ECB72;
    width: 100%;
    height: 35;
`