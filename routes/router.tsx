import * as React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from "../pages/Login";
import CreateAccount from "../pages/CreateAccount";



function Router (){
    const Stack = createNativeStackNavigator();
    return (
        <>
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen 
                name="Login" 
                component={Login}
                options = {{headerShown: false}}
                />
                <Stack.Screen 
                name="CreateAccount" 
                component={CreateAccount}
                options = {{headerShown: false}}
                />
            </Stack.Navigator>
        </NavigationContainer>
        </>
    )
}

export default Router();