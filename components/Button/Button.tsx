import React from "react"
import { ButtonCreateAccount } from "./style"


type ButtonInfo = {
    text : string;
}

export function Button({
    text
} : ButtonInfo){
    return (
        <ButtonCreateAccount>
            {text}
        </ButtonCreateAccount>
    )
};