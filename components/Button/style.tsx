import styled from "styled-components/native";

export const ButtonCreateAccount = styled.View`
    font-family: 'Roboto';
    font-size: 30;
    background-color: #2ECB72;
    width: 59%;
    justify-content: center;
    align-items: center;
    margin-left: 21.4%;
    border-radius: 10;
    margin-top: 1rem;
    margin-bottom: 0.5rem;
    padding: 0.5rem;
`;