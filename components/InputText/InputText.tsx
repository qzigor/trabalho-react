import React from "react"
import { AiFillEyeInvisible } from "react-icons/ai";
import { SectionInput, TextDaView, Input } from "./style"


type InputTextInfo = {
    text : string;
    isPassword : boolean;
};


export function InputText({
    text,
    isPassword
} : InputTextInfo){
    let textPlaceHolder : string
    switch (text) {
        case 'Nome':
            textPlaceHolder = 'Digite seu nome';
            isPassword = false;
            break;
        case 'Senha':
            textPlaceHolder = 'Digite sua senha';
            isPassword = true;
            break;
        case 'Confirme sua Senha':
            textPlaceHolder = 'Digite sua senha';
            isPassword = true;
            break;
        case 'E-mail':
            textPlaceHolder = 'Digite seu e-mail';
            isPassword = false;
            break;
        default:
            break;
    }
    return (
        <>
            <TextDaView>
                {text}
            </TextDaView>

            {
                isPassword ? 
                <SectionInput>
                    <Input placeholder = {`${textPlaceHolder}`}/>
                    <AiFillEyeInvisible/>
                </SectionInput>
                :
                <SectionInput>
                    <Input placeholder = {`${textPlaceHolder}`}/>
                </SectionInput>
            }
        </>
    )
};