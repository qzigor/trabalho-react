import styled from "styled-components/native";
import { style } from "../../global/style";

export const SectionInput = styled.View`
    background-color: white;
    margin-left: 13.8%;
    margin-right: 13.8%;
    width: 72%;
    height: 4.3%;
    border-radius: 12;
    border-width: 2px;
    flex-direction: row;
    align-items: center;
    justify-content: space-evenly;
`;

export const Input = styled.TextInput`
    font-family: ${style.fonts.rb};
    font-size: 15;
    align-items: center;
`;

export const TextDaView = styled.Text`
    font-family: 'Poppins';
    font-size: 25;
    margin-top: 8%;
    margin-left: 14.4%;
    font-weight: bold;
`;

